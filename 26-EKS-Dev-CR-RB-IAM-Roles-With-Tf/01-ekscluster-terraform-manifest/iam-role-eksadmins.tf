data "aws_caller_identity" "current" {}

resource "aws_iam_role" "ekslist_role" {
  name = "ekslist-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          AWS = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
        }
      },
    ]
  })
  inline_policy {
    name = "eks-full-access-policy"

    policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
        {
            Action = [
                "iam:ListRoles",
                "eks:DescribeNodegroup",
                "eks:ListNodegroups",
                "eks:DescribeCluster",
                "eks:ListClusters",
                "eks:AccessKubernetesApi",
                "eks:ListUpdates",
                "eks:ListFargateProfiles",
                "eks:ListIdentityProviderConfigs",
                "eks:ListAddons",
                "eks:DescribeAddonVersions"
            ]
            Effect   = "Allow"
            Resource = "*"
        },
    ]
  })
  }
    tags = {
    Name = "ekslist-role"
  }
}  

