resource "kubernetes_role_binding_v1" "eksdeveloper_role_binding" {
  metadata {
    name      = "eksdeveloper-role-binding"
    namespace = "dev"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "${kubernetes_role_v1.eksdeveloper_role.metadata.0.name}"
  }
  subject {
    kind      = "Group"
    name      = "developer-read-write-on-dev-group" # Placeholder 
    api_group = "rbac.authorization.k8s.io"
  }
}