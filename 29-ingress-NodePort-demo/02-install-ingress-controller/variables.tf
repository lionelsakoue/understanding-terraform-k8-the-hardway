
variable "cluster_name" {
  type        = string
  description = "EKS cluster name"
  default     = "kojietchs-eks-demo-cluster"
}

variable "component_name" {
  description = "The name of the component, if applicable."
  type        = string
  default     = "kojietchs-eks-demo"
}

# EKS OIDC ROOT CA Thumbprint - valid until 2037
variable "eks_oidc_root_ca_thumbprint" {
  type        = string
  description = "Thumbprint of Root CA for EKS OIDC, Valid until 2037"
  default     = "9e99a48a9960b14926bb7f3b02e22da2b0ab7280"
}

variable "aws_region" {
    type = string
    description = "(optional) describe your variable"
    default = "us-east-1"
}
