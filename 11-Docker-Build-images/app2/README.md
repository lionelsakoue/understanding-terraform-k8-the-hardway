
### Command to Build Docker Images: 
```
docker login
docker build -t kojibello/kojitechs-app2-recap:v1.0.0 .
docker push kojibello/kojitechs-app2-recap:v1.0.0
```
## TEST DOCKER IMAGE
```
docker pull kojibello/kojitechs-app2-recap:v1.0.0
docker run --name my-app2 -p 80:80 -d kojibello/kojitechs-app2-recap:v1.0.0
```
