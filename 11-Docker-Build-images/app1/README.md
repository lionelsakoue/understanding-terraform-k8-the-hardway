
### Command to Build Docker Images: 
```
docker login
docker build -t kojibello/kojitechs-app-recap1:v1.0.0 .
docker push kojibello/kojitechs-app-recap1:v1.0.0
```
## TEST DOCKER IMAGE
```
docker run --name my-app-1 -p 80:80 -d kojibello/kojitechs-app-recap1:v1.0.0
```
