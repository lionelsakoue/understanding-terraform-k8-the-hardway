<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>

</div>

<div class="jumbotron">
  <h1 align="center">Kubernetes on Cloud</h1>
  <p align="center">Masterclass Series on Terraform, Docker and K8s</p>
</div>

<div class="container">
  <h2>Registration App</h2>
  <a href="/list-users" class="btn btn-info" role="button">List Users</a>
  <a href="/add-user" class="btn btn-info" role="button">Create Users</a>
</div>



<%@ include file="common/footer.jspf"%>