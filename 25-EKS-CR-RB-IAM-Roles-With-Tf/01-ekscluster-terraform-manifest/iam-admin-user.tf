
resource "aws_iam_user" "admin_user" {
  name = "eksadmin"
  path = "/"

  force_destroy = true
}

resource "aws_iam_user_policy_attachment" "admin_user" {
  user       = aws_iam_user.admin_user.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}