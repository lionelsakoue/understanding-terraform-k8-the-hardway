
resource "kubernetes_cluster_role_binding_v1" "eks_users_list_clusterrole_binding" {
  metadata {
    name = "eks-users-list-clusterrole-binding"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role_v1.eks_users_list_clusterrole.metadata.0.name
  }
  subject {
    kind      = "Group"
    name      = "eks-list-group" # Placeholder
    api_group = "rbac.authorization.k8s.io"
  }
}