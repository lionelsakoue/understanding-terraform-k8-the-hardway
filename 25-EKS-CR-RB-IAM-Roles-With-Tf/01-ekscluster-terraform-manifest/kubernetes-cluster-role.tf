
resource "kubernetes_cluster_role_v1" "eks_users_list_clusterrole" {
  metadata {
    name = "eks-users-list-clusterrole"
  }

  rule {
    api_groups = [""]
    # resources  = ["pods", "nodes","namespaces", "events", "configmaps", "services"]
    resources  = ["pods", "nodes","namespaces", "events", "services"]
    verbs      = ["get", "list"]
  }
  rule {
    api_groups = ["apps"]
    resources  = ["daemonsets", "deployments","replicasets", "statefulsets"]
    verbs      = ["get", "list"]
  }
  rule {
    api_groups = ["batch"]
    resources  = ["jobs"]
    verbs      = ["get", "list"]
  }
}

