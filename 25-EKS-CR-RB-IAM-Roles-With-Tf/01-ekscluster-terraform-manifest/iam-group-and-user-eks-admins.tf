
resource "aws_iam_group_policy" "eks_list_group_policy" {
  name  = "eks-list-group-policy"
  group = aws_iam_group.eks_list_group.name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "sts:AssumeRole",
        ]
        Effect   = "Allow"
        Sid      = "AllowAssumeOrganizationAccountRole"
        Resource = "${aws_iam_role.ekslist_role.arn}" # TODO
      },
    ]
  })
}

resource "aws_iam_group" "eks_list_group" {
  name = "eks-list-group"
  path = "/"
}

resource "aws_iam_user" "eks-list-users" {
for_each = toset(["kxb420", "kxb421", "kxb422", "kxb423"])

  name = each.value
  force_destroy = true
  path = "/"
}

resource "aws_iam_group_membership" "this" {
  name = "eks-list-group-membership"
 
 users = [ for user in aws_iam_user.eks-list-users: user.name]
  group = aws_iam_group.eks_list_group.name
}

