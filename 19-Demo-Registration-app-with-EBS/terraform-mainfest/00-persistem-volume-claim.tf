
resource "kubernetes_persistent_volume_claim_v1" "mysql_pv_claim" {
  metadata {
    name = "ebs-mysql-pv-claim"
  }
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "10Gi"
      }
    }
storage_class_name = kubernetes_storage_class_v1.mysql_sc.metadata.0.name
  }
}
