
variable "vpc_cidr_block" {
  description = "vpc cidr block."
  default     = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  description = "value for subnet cidr"
  default     = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24", "10.0.6.0/24"]
}

variable "private_subnet_cidr" {
  type        = list(any)
  description = "Value of subnet 1 cidr"
  default = [
    "10.0.1.0/24",
    "10.0.3.0/24",
    "10.0.5.0/24",
    "10.0.7.0/24"
  ]
}

# variable "subnet_1_az" {
#     type = string
#     description = "Value for subnet 1 az"
#     default = "us-east-1a"
# }

# variable "subnet_2_az" {
#     type = string
#     description = "Value for subnet 2 az"
#     default = "us-east-1b" 
# }