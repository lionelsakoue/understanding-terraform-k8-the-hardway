
################################
# COUNT FOR META - ARGUMENTS
################################

# creating vpc 
resource "aws_vpc" "aws_vpc" {

  cidr_block = var.vpc_cidr_block
  tags = {
    Name = "kojitechs-vpc"
  }
}

## "RESOURCE_NAME.LOCAL_NAME.DESIRED_ARTR"
resource "aws_subnet" "public_subnets" {
  count = length(var.public_subnet_cidr) # 4

  vpc_id                  = local.vpc_id
  cidr_block              = var.public_subnet_cidr[count.index]
  availability_zone       = data.aws_availability_zones.available.names[count.index] # ToDo (element)
  map_public_ip_on_launch = true                                                     #()

  tags = {
    Name = "public-subnets-${count.index + 1}"
  }
}
# ["1","2","3","4","5","6","7","8","9]

# aws_subnet.public_subnets.id
# splat operator / legacy splat operator

resource "aws_subnet" "private_subnets" {
  count = length(var.private_subnet_cidr)

  vpc_id            = local.vpc_id
  cidr_block        = var.private_subnet_cidr[count.index]
  availability_zone = data.aws_availability_zones.available.names[count.index] # remain thesame

  tags = {
    Name = "private-subnets-${count.index + 1}" # remain the same
  }
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ami.id
  instance_type = "t3.micro"
  subnet_id     = aws_subnet.public_subnets[0].id

  tags = {
    Name = "web"
  }
}
