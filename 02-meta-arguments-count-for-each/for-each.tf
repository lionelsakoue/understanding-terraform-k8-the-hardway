
################################
# for_each FOR META - ARGUMENTS
################################

# creating vpc 
resource "aws_vpc" "for_each_vpc" {

  cidr_block = var.vpc_cidr_block
  tags = {
    Name = "kojitechs-vpc"
  }
}

locals {
  #   public_subnet  = {
  #     pub_subnet_1 = {
  #        cidr_block = "10.0.0.0/24"
  #        az = "us-east-1a"
  #     }
  #     pub_subnet_2 = {
  #        cidr_block = "10.0.2.0/24"
  #        az = "us-east-1b"
  #     }
  #   }
  # private_subnet = {}
}

variable "public_subnets" {
  type        = map(any)
  description = "objects of public_subnets to be created."
  default = {
    public_subnet_3 = {
      cidr_block        = "10.0.4.0/24"
      availability_zone = "us-east-1a"
    }
    public_subnet_1 = {
      cidr_block        = "10.0.0.0/24"
      availability_zone = "us-east-1a"
    }
    public_subnet_2 = {
      cidr_block        = "10.0.2.0/24"
      availability_zone = "us-east-1b"
    }
  }
}

## "RESOURCE_NAME.LOCAL_NAME.DESIRED_ARTR"
resource "aws_subnet" "public_subnets_for_each" {

  for_each = var.public_subnets

  vpc_id                  = local.vpc_id
  cidr_block              = each.value.cidr_block
  availability_zone       = each.value.availability_zone
  map_public_ip_on_launch = true

  tags = {
    Name = each.key
  }
}

# resource "aws_subnet" "private_subnets_for_each" {
# #   count = length(var.private_subnet_cidr) # 3

#   vpc_id            = local.vpc_id
#   cidr_block        = var.private_subnet_cidr[count.index]
#   availability_zone = data.aws_availability_zones.available.names[count.index] # remain thesame

#   tags = {
#     Name = "private-subnets-${count.index + 1}" # remain the same
#   }
# }

# resource "aws_instance" "web_for_each" {
#   ami           = data.aws_ami.ami.id
#   instance_type = "t3.micro"
#   subnet_id = aws_subnet.public_subnets_for_each[0].id

#   tags = {
#     Name = "web"
#   }
# }
