
resource "aws_iam_group_policy" "eks_admin_group_policy" {
  name  = "eks-admin-group-policy"
  group = aws_iam_group.eks_admin_group.name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "sts:AssumeRole",
        ]
        Effect   = "Allow"
        Sid      = "AllowAssumeOrganizationAccountRole"
        Resource = "${aws_iam_role.eksadmin_role.arn}"
      },
    ]
  })
}

resource "aws_iam_group" "eks_admin_group" {
  name = "eks-admin"
  path = "/"
}

resource "aws_iam_user" "eks-admin-users" {
for_each = toset(["kxb420", "kxb421", "kxb422", "kxb423"])

  name = each.value
  path = "/"
}

resource "aws_iam_group_membership" "team" {
  name = "eks-admin-group-membership"
 
 users = [ for user in aws_iam_user.eks-admin-users: user.name]
  group = aws_iam_group.eks_admin_group.name
}

