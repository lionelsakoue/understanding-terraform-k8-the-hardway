
variable "vpc_cidr" {
  type        = string
  description = "vpc network cidr address for kojitechs vpc"
  # default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  type        = list(any)
  description = "List of public subnet"
}

variable "public_instance_type" {
  type        = string
  description = "Public instance type"
  default     = "t2.micro"
}

variable "private_instance_type" {
  type        = string
  description = "private instance type"
  default     = "t2.micro"
}

variable "registrationapp_instance_type" {
  type        = string
  description = "private instance type"
  default     = "t2.xlarge"
}

variable "db_name" {
  type        = string
  description = "value for database name"
  default     = "webappdb"
}

variable "instance_class" {
  type        = string
  description = "(optional) describe your variable"
  default     = "db.t2.micro"
}

variable "db_username" {
  type        = string
  description = "(optional) describe your variable"
  default     = "kojitechs"
}

variable "port" {
  type        = number
  description = "database port"
  default     = 3306
}

variable "dns_name" {
  description = "This is the dns name of that would be used to connect to all applications"
  type        = string
}

variable "subject_alternative_names" {
  type = list(any)
}

