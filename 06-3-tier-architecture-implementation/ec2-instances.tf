
data "aws_ami" "ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

################################################################################
# CREATING PUBLIC EC2 INSTANCE.
################################################################################

resource "aws_instance" "app1" {

  ami                  = data.aws_ami.ami.id
  instance_type        = var.public_instance_type
  subnet_id            = aws_subnet.private_subnet_1.id
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  security_groups      = [aws_security_group.static_sg.id] # 80 from who? alb
  user_data            = file("./templates/app1.sh")
  tags = {
    Name = "app1-${terraform.workspace}"
  }
    lifecycle {
    ignore_changes        = [security_groups] # this this to prevent our target group no to get destroyed
  }
}

################################################################################
# CREATING PRIVATE EC2 INSTANCE.
################################################################################

resource "aws_instance" "app2" {

  ami                  = data.aws_ami.ami.id
  instance_type        = var.private_instance_type
  subnet_id            = aws_subnet.private_subnet_1.id
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  security_groups      = [aws_security_group.static_sg.id] # # 80 from who? alb
  user_data            = file("${path.module}/templates/app2.sh")

  tags = {
    Name = "app2-${terraform.workspace}"
  }
    lifecycle {
    ignore_changes        = [security_groups] # this this to prevent our target group no to get destroyed
  }
}

################################################################################
# CREATING REGISTRATION APP.
################################################################################

resource "aws_instance" "registration_app" {
  depends_on = [aws_db_instance.registration_app_db]

  count         = 2
  ami           = data.aws_ami.ami.id
  instance_type = var.registrationapp_instance_type
  subnet_id     = [aws_subnet.private_subnet_1.id, aws_subnet.private_subnet_2.id][count.index]

  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  security_groups      = [aws_security_group.registration_app.id]
  user_data = templatefile("${path.module}/templates/registration_app.tmpl",
    {
      application_version = "v1.2.01"
      hostname            = "${aws_db_instance.registration_app_db.address}"
      port                = "${var.port}"
      db_name             = "${var.db_name}"
      db_username         = "${var.db_username}"
      db_password         = "${random_password.password.result}"
    }
  )
  tags = {
    Name = "registration-app-${terraform.workspace}"
  }
   lifecycle {
    ignore_changes        = [security_groups] # this this to prevent our target group no to get destroyed
  }
}