
# Use this data source to lookup information about the current AWS partition in which Terraform is working
data "aws_partition" "current" {}


# Resource: AWS IAM Open ID Connect Provider
resource "aws_iam_openid_connect_provider" "oidc_provider" {
  client_id_list  = ["sts.${data.aws_partition.current.dns_suffix}"]
  thumbprint_list = [var.eks_oidc_root_ca_thumbprint]
  url             = data.aws_eks_cluster.eks-cluster.identity.0.oidc.0.issuer

  tags = merge(
    {
      Name = format("%s-cluster-eks-irsa", var.component_name)  
    },
    module.required_tags.tags
  )
}

# Resource: Create IAM Role and associate the S3 IAM Policy to it
resource "aws_iam_role" "irsa_iam_role" {
  name = "${var.cluster_name}-irsa-iam-role"

  # Terraform's "jsonencode" function converts a Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Federated = "${aws_iam_openid_connect_provider.oidc_provider.arn}"
        }
        Condition = {
          StringEquals = {  
            "${aws_iam_openid_connect_provider.oidc_provider.url}:sub": "system:serviceaccount:default:${var.service_account_name}"
          }
        }        
      },
    ]
  })
  tags = {
    Name = "${var.cluster_name}-irsa-iam-role"
  }
  
}

# Associate IAM Role and Policy
resource "aws_iam_role_policy_attachment" "irsa_iam_role_policy_attach" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
  role       = aws_iam_role.irsa_iam_role.name
}
