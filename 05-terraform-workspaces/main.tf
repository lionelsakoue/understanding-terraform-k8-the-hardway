################################################################
# Create 2 ec2 instances making use of count / for_each block
################################################################
# creating vpc 
resource "aws_vpc" "aws_vpc" {

  cidr_block = var.vpc_cidr # (prod, dev, sbx)
  tags = {
    Name = "kojitechs-vpc-${terraform.workspace}"
  }
} # 

## "RESOURCE_NAME.LOCAL_NAME.DESIRED_ARTR"
resource "aws_subnet" "subnet_1" {

  vpc_id                  = aws_vpc.aws_vpc.id
  cidr_block              = var.subnet_1_cidr # (prod, dev, sbx)
  availability_zone       = "us-east-1a"      # // data source availability zone
  map_public_ip_on_launch = true

  tags = {
    Name = "subnet-1-${terraform.workspace}"
  }
}

resource "aws_subnet" "subnet_2" {
  vpc_id                  = aws_vpc.aws_vpc.id
  cidr_block              = var.subnet_2_cidr # (prod, dev, sbx)
  availability_zone       = "us-east-1b"      # // data source availability zone
  map_public_ip_on_launch = true

  tags = {
    Name = "subnet-2-${terraform.workspace}"
  }
}

# count
resource "aws_instance" "this" {
  count = 2

  ami           = data.aws_ami.ami.id
  instance_type = "t3.micro" # (bigger instance type for prod use thesame instance type for prod use the same instance type dev, sbx)
  subnet_id     = [aws_subnet.subnet_1.id, aws_subnet.subnet_2.id][count.index]
  #  subnet_id = element([aws_subnet.subnet_1.id,aws_subnet.subnet_2.id], count.index)

  tags = {
    Name = "app-${count.index + 1}-${terraform.workspace}"
  }
}

### CHILD 

