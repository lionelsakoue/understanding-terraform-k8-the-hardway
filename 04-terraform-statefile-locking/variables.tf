
variable "bucket_name" {
  type        = list(any)
  description = "The name of the  state bucket"
  default = [
    "kojitechs.state.bucket",
    "06.3.tier.architecture.implementation"
  ]
}
