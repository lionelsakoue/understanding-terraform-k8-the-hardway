---
title: Demo on Kubernetes Storage Class, PVC and PV with Terraform
description: Deploy registration-app WebApp on EKS Kubernetes with MySQL as Database using Terraform
---

## Step-00: Introduction
- Create Terraform configs for following Kubernetes Resources
   - Kubernetes Storage Class
   - Kubernetes Persistent Volume Claim
   - Kubernetes Config Map
   - Kubernetes Deployment for MySQL DB
   - Kubernetes ClusterIP Service for MySQL DB
   - Kubernetes Deployment for User Management Web Application
   - Kubernetes Load Balancer (Classic LB) for UMS Web App
   - Kubernetes Load Balancer (Network LB) for UMS Web App
   - Kubernetes Node Port Service for UMS Web App


## Pre-requisite: Verify EKS Cluster and EBS CSI Driver already Installed
### Project-01: 01-ekscluster-terraform-manifests
```t
# Change Directroy
cd 16-EBS-Kubernetes-SampleApp-Terraform/01-ekscluster-terraform-manifests

# Terraform Initialize
terraform init

# List Terraform Resources (if already EKS Cluster created as part of previous section we can see those resources)
terraform state list

# Else Run below Terraform Commands
terraform validate
terraform plan
terraform apply -auto-approve

# Configure kubeconfig for kubectl
aws eks --region <region-code> update-kubeconfig --name <cluster_name>
aws eks --region us-east-1 update-kubeconfig --name hr-dev-eksdemo1

# Verify Kubernetes Worker Nodes using kubectl
kubectl get nodes
kubectl get nodes -o wide
```
### Project-02: 02-ebs-terraform-manifests
```t
# Change Directroy
cd 16-EBS-Kubernetes-SampleApp-Terraform/02-ebs-terraform-manifests

# Terraform Initialize
terraform init

# List Terraform Resources (if already EBS CSI Driver created as part of previous section we can see those resources)
terraform state list

# Else Run below Terraform Commands
terraform validate
terraform plan
terraform apply -auto-approve

# Configure kubeconfig for kubectl
aws eks --region <region-code> update-kubeconfig --name <cluster_name>
aws eks --region us-east-1 update-kubeconfig --name hr-dev-eksdemo1

# Verify EBS CSI Controller and Node pods running in kube-system namespace
kubectl -n kube-system get pods