
terraform {
  required_version = ">=1.1.0"

  backend "s3" {
    bucket         = "prod-nfor"
    key            = "path/evn/16-Registration-app-Demo/terraform-mainfest"
    region         = "us-east-1"
    dynamodb_table = "terraform-lock"
    encrypt        = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.55.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.20.0"
    }

  }
}

locals {
  endpoint               = data.aws_eks_cluster.eks-cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks-cluster.certificate_authority.0.data)
}

data "aws_eks_cluster" "eks-cluster" {
  name = var.cluster_name
}

data "aws_eks_cluster_auth" "this" {
  name = data.aws_eks_cluster.eks-cluster.name
}

provider "aws" {
  region = "us-east-1"
}

provider "kubernetes" {
  host                   = local.endpoint
  cluster_ca_certificate = local.cluster_ca_certificate
  token                  = data.aws_eks_cluster_auth.this.token
}
