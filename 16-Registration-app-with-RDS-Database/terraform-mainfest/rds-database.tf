
data "aws_vpc" "this" {

  filter {
    name   = "tag:Name"
    values = ["kojietchs-eks-demo-vpc"]
  }
}

data "aws_subnets" "database_subnets" {

  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.this.id]
  }
  tags = {
    Name = "database-subnets"
  }
}

locals {
  db_credentials = {
    endpoint    = aws_db_instance.registration_app_db.address
    db_username = var.db_username
    db_name     = var.db_name
    port        = var.port
    password    = random_password.password.result
  }
  db_secrets = jsondecode(data.aws_secretsmanager_secret_version.this.secret_string)
}

resource "aws_secretsmanager_secret" "this" {
  name                    = "kojitechs-mysql-secrets-mysql-${terraform.workspace}"
  description             = "Secret to manage mysql superuser password"
  recovery_window_in_days = 0
}

resource "aws_secretsmanager_secret_version" "this" {
  secret_id     = aws_secretsmanager_secret.this.id
  secret_string = jsonencode(local.db_credentials)
}

resource "random_password" "password" {
  length  = 16
  special = false
}

resource "aws_db_subnet_group" "mysql_subnetgroup" {
  name       = "registrationapp-subnetgroup-${terraform.workspace}"
  subnet_ids = data.aws_subnets.database_subnets.ids

  tags = {
    Name = "registrationapp-subnetgroup-${terraform.workspace}"
  }
}

################################################################################
# CREATING  DATABASE SECURITY GROUP.
################################################################################

resource "aws_security_group" "database_security_group" {
  name        = "mysql-sg-${terraform.workspace}"
  description = "Allow inboun from from registration app security group"
  vpc_id      = data.aws_vpc.this.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "mysql-sg-${terraform.workspace}"
  }
}

data "aws_security_group" "jumpserver_security_group_id" {
  name = "jump-server"
}

data "aws_security_group" "eks_cluster_security_group_id" {
  name = "eks-cluster-sg-kojietchs-eks-demo-cluster-*"
}

################################################################################
# CREATING  REGISTRATION APP SECURITY GROUP RULE.
################################################################################

resource "aws_security_group_rule" "mysql_inboud_security_group" {

  security_group_id        = aws_security_group.database_security_group.id
  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = data.aws_security_group.eks_cluster_security_group_id.id 
}

resource "aws_security_group_rule" "jumpserver_inboud_security_group" {

  security_group_id        = aws_security_group.database_security_group.id
  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = data.aws_security_group.jumpserver_security_group_id.id 
}

resource "aws_db_instance" "registration_app_db" {

  allocated_storage      = 20
  db_name                = var.db_name # "webappdb" # application team (webappdb)
  engine                 = "mysql"
  instance_class         = "db.t3.micro"
  username               = var.db_username
  port                   = var.port
  password               = random_password.password.result
  db_subnet_group_name   = aws_db_subnet_group.mysql_subnetgroup.name
  skip_final_snapshot    = true
  vpc_security_group_ids = [aws_security_group.database_security_group.id]
}

