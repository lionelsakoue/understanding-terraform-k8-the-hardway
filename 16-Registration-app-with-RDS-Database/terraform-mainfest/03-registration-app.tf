
data "aws_secretsmanager_secret_version" "this" {
  depends_on = [aws_secretsmanager_secret_version.this]
  secret_id  = aws_secretsmanager_secret.this.id
}

resource "kubernetes_deployment_v1" "registration_app" {
  depends_on = [aws_db_instance.registration_app_db]

  metadata {
    name = "my-registration-app"
    labels = {
      app  = "registration-app"
      tier = "front-end"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "registration-app"
      }
    }
    template {
      metadata {
        labels = {
          app = "registration-app"
        }
      }
      spec {
        container {
          name  = "registration-app"
          image = format("kojibello/kojitechs-registration-app:%s", var.image_version)
          port {
            container_port = "8080"
          }
          env {
            name  = "DB_HOSTNAME"
            value = local.db_secrets["endpoint"]
          }
          env {
            name  = "DB_PORT"
            value = local.db_secrets["port"]
          }
          env {
            name  = "DB_NAME"
            value = local.db_secrets["db_name"]
          }
          env {
            name  = "DB_USERNAME"
            value = local.db_secrets["db_username"]
          }
          env {
            name  = "DB_PASSWORD"
            value = local.db_secrets["password"]
          }
        }
      }
    }
  }
}
