
## Step-00: Introduction

- Understand k8s Environment Variables
- **Usecase:** Deploy User Management Application on Kubernetes with EBS as storage for MySQL Pod

## Pre-requisite: Verify EKS Cluster 
### Project-01: 01-ekscluster-terraform-manifests
```t
# Change Directroy
cd 12-Create-eks-cluster

# Terraform Initialize
terraform init

# List Terraform Resources (if already EKS Cluster created as part of previous section we can see those resources)
terraform state list

# Else Run below Terraform Commands
terraform validate
terraform plan
terraform apply -auto-approve

# Configure kubeconfig for kubectl
aws eks --region <region-code> update-kubeconfig --name <cluster_name>
aws eks --region us-east-1 update-kubeconfig --name kojietchs-eks-demo-cluster

# Verify Kubernetes Worker Nodes using kubectl
kubectl get nodes
kubectl get nodes -o wide
```

## 01-mysql-deployment.yaml
- **Folder:** `01-mysql-deployment.yaml`
```yaml
apiVersion: apps/v1
kind: Deployment
metadata: 
  name: mysql
  labels: 
    app: mysql
    tier: backend

spec:
  replicas: 1
  selector:
    matchLabels:
      app: mysql
  strategy:
    type: Recreate

  template:
    metadata:
      name: mysql
      labels: # DICTIONARY
        app: mysql
    spec:
      containers: 
        - name: mysql
          image: mysql:5.6
          ports: 
            - containerPort: 3306
          env: 
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mysql
                  key: password
          volumeMounts:
            - name: mysql-dbcreation-script
              mountPath: /docker-entrypoint-initdb.d  

      volumes:
        # - name:  mysql-dbcreation-script
        #   persistentVolumeClaim:
        #     claimName: ebs-mysql-volume
        - name:  mysql-dbcreation-script
          configMap:  
            name: mysql-dbcreation-script 
```

## 02-mysql-clusterIP-service.yaml

```yaml
apiVersion: v1
kind: Service
metadata: 
  name: mysql

spec:
  selector:
    app: mysql
  ports:
    - port: 3306 
  clusterIP: None # This means we are going to use Pod IP
```

## Step-06: 01-mysql-deployment.yaml

```yaml
apiVersion: apps/v1
kind: Deployment
metadata: 
  name: my-registration-app 
  labels: 
    app: registration-app
    tier: front-end

spec:
  replicas: 2
  selector:
    matchLabels:
      app: registration-app
  template:
    metadata:
      name: registration-app
      labels: # DICTIONARY
        app: registration-app
    spec:
      containers: 
        - name: registration-app 
          image: kojibello/kojitechs-registration-app:f56f73ef6a4a7fb
          ports: 
            - containerPort: 8080
          env: 
            - name: DB_HOSTNAME
              value: "mysql"
            - name: DB_PORT
              value: "3306"
            - name: DB_NAME
              value: "webappdb" # MANDATORY
            - name: DB_USERNAME
              value: "root"
            - name: DB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mysql
                  key: password

```

## Step-07: alb-service-demo.yaml
- **Folder:** `15-EBS-Kubernetes-SampleApp-YAML/03-kube-manifests-UMS-WebApp`
```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-alb-service
  
spec:
  type: LoadBalancer # Clasic (application alb)
  selector:
    app: registration-app
  ports:
    - name: http 
      port: 80 
      targetPort: 8080

```

## Step-08: nlb-service-demo.yaml

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-nlb-service
  annotations:
    service.beta.kubernetes.io/aws-load-balancer-type: nlb

spec:
  type: LoadBalancer
  selector:
    app: registration-app
  ports:
    - name: http 
      port: 80 
      targetPort: 8080

```

## Step-09: nodePort-service-demo.yaml

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-nodeport-service
  
spec:
  type: NodePort
  selector:
    app: registration-app
  ports:
    - name: http 
      port: 80 
      targetPort: 8080
      # nodePort: 31542
```

## nodePort-service-demo.yaml Application
```t
# Change Directory
cd 15-EBS-Kubernetes-SampleApp-YAML/

# Deploy Application using kubectl
kubectl apply -f 03-kube-manifests-UMS-WebApp
```

## Verify Deployments
kubectl get deploy
Observation:
1. We should see both deployments in default namespace
- mysql
- usermgmt-webapp

## Verify Pods
kubectl get pods
Observation:
1. You should see both pods running

## Describe both pods and review events
kubectl describe pod <POD-NAME>
kubectl describe pod mysql-6fdd448876-hdhnm
kubectl describe pod usermgmt-webapp-cfd4c7-fnf9s

## Review registration-app Pod Logs
kubectl logs -f usermgmt-webapp-cfd4c7-fnf9s
Observation:
1. Review the logs and ensure it is successfully connected to MySQL POD

## Verify Services
kubectl get svc
```

## Step-12: Connect to MySQL Database Pod
```t
# Connect to MySQL Database 
kubectl run -it --rm --image=mysql:5.6 --restart=Never mysql-client -- mysql -h mysql -pPassword$

# Verify usermgmt schema got created which we provided in ConfigMap
mysql> show schemas;
mysql> use webappdb;
mysql> show tables;
mysql> select * from user;
## Create user
```bash
INSERT INTO user (userid, email_address, first_name, last_name, password, ssn, user_name)
VALUES ("101", "kojitechs@gmail.com", "koji", "bello", "$2a$10$w.2Z0pQl9K5GOMVT.y2Jz.UW4Au7819nbzNh8nZIYhbnjCi6MG8Qu", "202XXX", "kojitechs");
INSERT INTO role (roleid, role) VALUES ("201", "ADMIN");
INSERT INTO user_role (userid, roleid) VALUES ("101", "201");
```

## Sample Output for above query
+--------+----------------------------+------------+-----------+--------------------------------------------------------------+--------+-----------+
| userid | email_address              | first_name | last_name | password                                                     | ssn    | user_name |
+--------+----------------------------+------------+-----------+--------------------------------------------------------------+--------+-----------+
|    101 | admin101@stacksimplify.com | Kalyan     | Reddy     | $2a$10$w.2Z0pQl9K5GOMVT.y2Jz.UW4Au7819nbzNh8nZIYhbnjCi6MG8Qu | ssn101 | admin101  |
+--------+----------------------------+------------+-----------+--------------------------------------------------------------+--------+-----------+
1 row in set (0.00 sec)
mysql> 

Observation:
1. If registration-app WebApp container successfully started, it will connect to Database and create the default user named admin101
Username: admin101
Password: password101
```
## Step-13: Access Sample Application
```t
# Verify Services
kubectl get svc

# Access using browser
http://<CLB-DNS-URL>
http://<NLB-DNS-URL>
Username: admin101
Password: password101

# Create Users and Verify using registration-app WebApp in browser
admin102/password102
admin103/password103

# Verify the same in MySQL DB
## Connect to MySQL Database 
kubectl run -it --rm --image=mysql:5.6 --restart=Never mysql-client -- mysql -h mysql -pdbpassword11

## Verify usermgmt schema got created which we provided in ConfigMap
mysql> show schemas;
mysql> use webappdb;
mysql> show tables;
mysql> select * from user;
```



## Step-14: Node Port Service Port - Update Node Security Group
- **Important Note:** This is not a recommended option to update the Node Security group to open ports to internet, but just for learning and testing we are doing this. 
- Go to Services -> Instances -> Find Private Node Group Instance -> Click on Security Tab
- Find the Security Group with name `eks-remoteAccess-`
- Go to the Security Group (Example Name: sg-027936abd2a182f76 - eks-remoteAccess-d6beab70-4407-dbc7-9d1f-80721415bd90)
- Add an additional Inbound Rule
   - **Type:** Custom TCP
   - **Protocol:** TCP
   - **Port range:** 31280
   - **Source:** Anywhere (0.0.0.0/0)
   - **Description:** NodePort Rule
- Click on **Save rules**


## Step-15: Access Sample using NodePort Service 
```t
# List Nodes
kubectl get nodes -o wide
Observation: Make a note of the Node External IP

# List Services
kubectl get svc
Observation: Make a note of the NodePort service port "myapp1-nodeport-service" which looks as "80:31280/TCP"

# Access the Sample Application in Browser
http://<EXTERNAL-IP-OF-NODE>:<NODE-PORT>
http://54.165.248.51:31280
Username: admin101
Password: password101
```

## Step-16: Remove Inbound Rule added  
- Go to Services -> Instances -> Find Private Node Group Instance -> Click on Security Tab
- Find the Security Group with name `eks-remoteAccess-`
- Go to the Security Group (Example Name: sg-027936abd2a182f76 - eks-remoteAccess-d6beab70-4407-dbc7-9d1f-80721415bd90)
- Remove the NodePort Rule which we added.

## Step-17: Clean-Up
```t
# Delete Kubernetes  Resources
kubectl delete -f kube-manifests-UMS-WebApp

# Verify Kubernetes Resources
kubectl get pods
kubectl get svc
```

# https://181437319056.signin.aws.amazon.com/console