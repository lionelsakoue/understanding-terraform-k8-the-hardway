
################################################################
# SINCE VPC AND COMPONENT HAS BEEN DEPLOYED, WE'D FETCH ART USING terraform_remote_state
################################################################

data "terraform_remote_state" "operational_network" {
  backend = "s3"

  config = {
    region = "us-east-1"
    bucket = "terrform-remote-state-datasource"
    key    = format("env:/%s/path/evn", terraform.workspace) # "env:/${terraform.workspace}/path/evn"
  }
}
################################################################

locals {
  operation_network = data.terraform_remote_state.operational_network.outputs
  vpc_id            = local.operation_network.vpc_id
  public_subnet     = local.operation_network.public_subnet
  private_subnet    = local.operation_network.private_subnet
  database_subnet   = local.operation_network.database_subnet
}