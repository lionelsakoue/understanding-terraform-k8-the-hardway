Building a highly available 3-tier application on AWS using terraform end-to-end.

proceduces
- create a new repo/existing repository
- create s3 bucket/dynamodb table for the backend 
- configure the backend(terraform.tf)
- creating workspace

```
terraform init
terraform workspace new sbx
terraform workspace list
```
## Run terraform plan/apply/destroy
```
terraform plan -var-file sbx.tfvars
```