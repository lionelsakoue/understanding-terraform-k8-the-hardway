
data "aws_ami" "ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

## pulling down keypair 

data "aws_key_pair" "my_key_pair" {
  key_name           = var.key_name
  include_public_key = true

  #   filter {
  #     name   = "tag:Name"
  #     values = ["kojitechs-keypair"]
  #   }
}

