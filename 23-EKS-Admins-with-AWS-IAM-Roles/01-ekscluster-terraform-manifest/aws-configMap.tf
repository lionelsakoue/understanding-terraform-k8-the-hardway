

data "aws_eks_cluster_auth" "this" {
  name = aws_eks_cluster.this.id
}

provider "kubernetes" {
  host                   = aws_eks_cluster.this.endpoint
  cluster_ca_certificate = base64decode(aws_eks_cluster.this.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.this.token
}

locals {
  config_mapRoles = [
    {
      rolearn  = "${aws_iam_role.eks_nodegroup.arn}"
      username = "system:node:{{EC2PrivateDNSName}}"
      groups   = ["system:bootstrappers", "system:nodes"]
  }
  ]
  config_mapusers = [
    {
      userarn  = "${aws_iam_user.admin_user.arn}"
      username = "eksadmin"
      groups   = ["system:masters"]
    },
    {
      userarn  = "${aws_iam_user.basic_user.arn}"
      username = "basicuser"
      groups   = ["system:masters"]
    },
  ]
}

resource "kubernetes_config_map_v1" "aws_auth" {
  depends_on = [aws_eks_cluster.this]

  metadata {
    name      = "aws-auth"
    namespace = "kube-system"
  }

  data = {
    mapRoles = yamlencode(local.config_mapRoles)
    mapUsers = yamlencode(local.config_mapusers)
  }
}